#!/bin/bash

targets=("linux-x64:" "win-x64:" "linux-x64:portable" "win-x64:portable")
ver=1.7.0
rev=$(( $(git rev-list --count HEAD) + 1 ))
fullver="1.0.0.0"

for t in ${targets[@]}
do
	target="$(echo $t | cut -d':' -f1 )"
	mode="$(echo $t | cut -d':' -f2 )"
	if [ ! -z "${mode}" ]; then
		mode="-${mode}"
	fi
	echo ""
	echo "PUBLISHING: ${target} ${mode}"
	rm -r "./bin/Release"
	rm -r "./obj/Release/netcoreapp3.1/${t}"
	if [ "${mode}" == "-portable" ]; then
		rm "./publish/FakeBDFolderThingy-v${fullver}-${target}${mode}"
		dotnet publish -r "${target}" -c Release --self-contained true -p:PublishSingleFile=true -p:PublishTrimmed=true 
	else
		rm "./publish/FakeBDFolderThingy-v${fullver}-${target}"
		dotnet publish -r "${target}" -c Release --self-contained false -p:PublishSingleFile=true
	fi
	rm "./bin/Release/netcoreapp3.1/${target}/publish/FakeBDFolderThingy.pdb"
	for file in $(find "./bin/Release/netcoreapp3.1/${target}/publish" | awk "NR>1")
	do
		mv -v "${file}" "publish/`echo "$file" | awk -F'/' '{print $NF}' | sed "s/FakeBDFolderThingy/FakeBDFolderThingy-v${fullver}-${target}${mode}/g"`"
	done
done