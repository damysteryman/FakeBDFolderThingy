using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace FakeBDFolderThingy
{
    public class Utils
    {
        public static string GetMakemkvPath(string _path = "")
        {
            string _exe = "makemkvcon";
            string[] makemkvPaths =
            {
                _path,
                $@"{Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)}\MakeMKV\{_exe}64.exe",
                $@"{Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)}\MakeMKV\{_exe}.exe",
                $@"{_exe}",
                $@"/usr/bin/{_exe}",
                $@"MakeMKV\{_exe}.exe",
            };

            foreach (string mp in makemkvPaths)
                if (File.Exists(mp))
                {
                    _path = mp;
                    break;
                }

            if (File.Exists(_path))
            {
                return _path;
            }
            else
            {
                string errMsg = "ERROR!" + Environment.NewLine +
                                $"Program '{_exe}' not found!" + Environment.NewLine +
                                Environment.NewLine +
                                $"Default locations for {_exe} are:" + Environment.NewLine +
                                Environment.NewLine +
                                "WINDOWS:" +
                                $@"<Program Files (x86)>\MakeMKV\{_exe}64.exe" + Environment.NewLine +
                                $@"<Program Files (x86)>\MakeMKV\{_exe}.exe" + Environment.NewLine +
                                $@"<Current Working Dir>\MakeMKV\{_exe}.exe" + Environment.NewLine +
                                Environment.NewLine +
                                "LINUX:" + Environment.NewLine +
                                $"/usr/bin/{_exe}" + Environment.NewLine +
                                $"<Current Working Dir>/{_exe}" + Environment.NewLine +
                                Environment.NewLine + Environment.NewLine +
                                $"Custom path to {_exe} executable can be set with the --makemkv argument." + Environment.NewLine +
                                $"{_exe} is part of the MakeMKV available at https://www.makemkv.com/download/" + Environment.NewLine +
                                Environment.NewLine;

                throw new FileNotFoundException(errMsg, _path);
            }
        }

        public static Tuple<int, string> RunProcess(string exeName, string args, bool verbose = false)
        {
            using (Process process = Process.Start(new ProcessStartInfo
            {
                FileName = exeName,
                Arguments = args,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                CreateNoWindow = true
            }))
            {
                using (StreamReader streamReader = process.StandardOutput)
                {
                    List<string> output = new List<string>();
                    while (!streamReader.EndOfStream)
                    {
                        string line = streamReader.ReadLine();
                        output.Add(line);
                        if (verbose) Console.WriteLine(line);
                    }
                    process.WaitForExit();
                    return new Tuple<int, string>(process.ExitCode, String.Join(Environment.NewLine, output));
                }
            }
        }
    }
}