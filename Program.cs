﻿using Exts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Linq;

namespace Exts
{
    public static class ExtMethods
    {
        public static string ShaveDirSeparator(this string str)
        {
            return (str.EndsWith(Path.DirectorySeparatorChar.ToString(), StringComparison.CurrentCulture)) ?
                str = str.Remove(str.Length - 1, 1) : str;
        }
    }
}

namespace FakeBDFolderThingy
{
    class Program
    {
        static bool overwrite = false;
        static bool verbose = false;
        static string src = "";
        static string dest = "";
        static string makemkv = "";
        static readonly char dsc = Path.DirectorySeparatorChar;
        public static void Banner()
        {
            string str = $"= Ver: {Assembly.GetExecutingAssembly().GetName().Version.ToString()}";
            while (str.Length < 24)
                str += " ";

            Console.WriteLine("=========================");
            Console.WriteLine("= Fake BD Folder Thingy =");
            Console.WriteLine($"{str}=");
            Console.WriteLine("=========================");
        }

        public static void Help()
        {
            Console.WriteLine("Creates a \"clone\" folder of a Blu-ray structure, but with split/edited playlist files." + Environment.NewLine);
            Console.WriteLine("Usage: FakeBDFolderThingy -s <source dir>");
            Console.WriteLine("Options:");
            Console.WriteLine("\t-h | --help                Show this help and exit.");
            Console.WriteLine("\t-s | --src     <src dir>   Source BDMV directory structure to \"clone\".");
            Console.WriteLine("\t-d | --dest    <dest dir>  Destination path to place the \"clone\" directory.");
            Console.WriteLine("\t-f | --force               Force overwrite \"clone\" directory if it already exists.");
            Console.WriteLine("\t-m | --makemkv <path>      Custom path to 'makemkvcon' binary.");
        }

        [DllImport("kernel32.dll", EntryPoint = "CreateSymbolicLinkW", CharSet = CharSet.Unicode, SetLastError = true)]
        static extern int CreateSymbolicLink(string lpSymlinkFileName, string lpTargetFileName, SymbolicLink dwFlags);
        static bool SymLink(string lpSymlinkFileName, string lpTargetFileName, SymbolicLink dwFlags)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                try
                {
                    if (CreateSymbolicLink(lpSymlinkFileName, lpTargetFileName, dwFlags) == 1)
                        return true;
                    else
                        throw new Win32Exception(Marshal.GetLastWin32Error());
                }
                catch (Exception e)
                {
                    Console.WriteLine($"ERROR creating symlink to {lpTargetFileName}: {e.Message}");
                    Console.WriteLine(e.StackTrace);
                    Console.WriteLine("Please ensure you have the necessary user permissions to create symlinks.");
                    Console.WriteLine("(Such as running this program as Administrator, or enabling Developer Mode)");
                    return false;
                }
            else
                return Utils.RunProcess("ln", $"-s \"{lpTargetFileName}\" \"{lpSymlinkFileName}\"").Item1 == 0;
        }
        static void MakemkvDiscattCreate(string src, string dest, string makemkv_path)
        {
            string device = src;
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                device = Utils.RunProcess("mount", "-l").Item2
                            .Split(Environment.NewLine)
                            .FirstOrDefault(s => s.Contains(src))
                            .Split(' ')[0];
            }
            string drvId = Utils.RunProcess(makemkv, "-r --cache=1 --noscan info disc:9999").Item2
                            .Split(Environment.NewLine)
                            .FirstOrDefault(s => s.Contains(device))
                            .Split(',')[0]
                            .Split(':')[1];

            if (Directory.Exists("temp")) Directory.Delete("temp", true);
            using (Process process = Process.Start(new ProcessStartInfo
            {
                FileName = makemkv_path,
                Arguments = $"backup --cache=16 --noscan -r --messages=-null --progress=-same disc:{drvId} temp",
                UseShellExecute = false,
                RedirectStandardOutput = false,
                CreateNoWindow = false
            }))
            {
                string discatt = $"temp{Path.DirectorySeparatorChar}discatt.dat";
                while (!File.Exists(discatt)) { }
                process.Kill();
            }
            File.Move($"temp{Path.DirectorySeparatorChar}discatt.dat", $"{dest}{Path.DirectorySeparatorChar}discatt.dat");
            Directory.Delete("temp", true);
        }
        enum SymbolicLink
        {
            File = 0,
            Directory = 1,
            FileUnprivileged = 2,
            DirectoryUnprivileged = 3
        }

        static void Main(string[] args)
        {
            Banner();

            if (args.Length > 0)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    switch (args[i])
                    {
                        case "--help":
                        case "-h":
                            Help();
                            return;

                        case "--force":
                        case "-f":
                            overwrite = true;
                            break;

                        case "--src":
                        case "-s":
                            src = args[i + 1].ShaveDirSeparator();
                            i++;
                            break;

                        case "--dest":
                        case "-d":
                            dest = args[i + 1].ShaveDirSeparator();
                            i++;
                            break;

                        case "--makemkv":
                        case "-m":
                            makemkv = args[i + 1].ShaveDirSeparator();
                            break;

                        case "--verbose":
                        case "-v":
                            verbose = true;
                            break;
                    }
                }

                Console.Write($"Looking for 'makemkvcon' app on system... ");
                try
                {
                    makemkv = Utils.GetMakemkvPath(makemkv);
                    Console.WriteLine($"OK! [{makemkv}]");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return;
                }

                if (string.IsNullOrEmpty(src))
                {
                    Console.WriteLine("ERROR: No source specified, nothing to do, exiting...");
                    return;
                }

                DriveInfo drive = DriveInfo.GetDrives()
                                        .Where(di => di.Name.StartsWith(src)).ToArray()
                                        .FirstOrDefault();
                if (string.IsNullOrEmpty(dest))
                    dest = Path.GetFileName(drive == null ? src : drive.VolumeLabel);

                if (Directory.Exists(src) && Directory.EnumerateFileSystemEntries(src).Contains(Path.Combine(src, "BDMV")))
                {
                    try
                    {
                        if (Directory.Exists(dest))
                            if (overwrite)
                                Directory.Delete(dest, true);
                            else
                            {
                                Console.WriteLine("ERROR: Destination directory already exists.");
                                return;
                            }
                        Directory.CreateDirectory(dest);
                        
                        Console.WriteLine("Creating Symlinks...");
                        foreach (string d in new string[] { "AACS", "CERTIFICATE" })
                            if (Directory.Exists(Path.Combine(src, d)))
                                if (!SymLink(Path.Combine(dest, d), Path.Combine(src, d), SymbolicLink.DirectoryUnprivileged))
                                    return;
                        Directory.CreateDirectory($"{dest}{dsc}BDMV");
                        foreach (string d in new string[] { "AUXDATA", "BACKUP", "BDJO", "CLIPINF", "JAR", "META", "STREAM" })
                        if (Directory.Exists(Path.Combine(src, "BDMV", d)))
                            if (!SymLink(Path.Combine(dest, "BDMV", d), Path.Combine(src, "BDMV", d), SymbolicLink.DirectoryUnprivileged))
                                return;
                        foreach (string f in new string[] { "index.bdmv", "MovieObject.bdmv" })
                            if (!SymLink(Path.Combine(dest, "BDMV", f), Path.Combine(src, "BDMV", f), SymbolicLink.FileUnprivileged))
                                return;

                        Console.WriteLine("Copying Playlists...");
                        string plist_dir = Path.Combine(dest, "BDMV", "PLAYLIST");
                        Directory.CreateDirectory($"{plist_dir}");
                        foreach (string path in Directory.GetFiles(Path.Combine(src, "BDMV", "PLAYLIST")))
                        {
                            string destfile = Path.Combine(plist_dir, Path.GetFileName(path));
                            File.Copy(path, destfile);
                            File.SetAttributes(destfile, FileAttributes.Normal);
                        }

                        Console.WriteLine("Splitting Playlists...");
                        Utils.RunProcess("BlurayMplsBatchSplitterThingy", $"\"{plist_dir}\" -d", verbose);

                        if (drive != null && drive.DriveType == DriveType.CDRom)
                        {
                            Console.WriteLine($"Source \"{src}\" is a Blu-ray disc, creating discatt.dat...");
                            MakemkvDiscattCreate(src, dest, makemkv);
                        }

                        Console.WriteLine("Fake BD Folder Creation DONE.");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.WriteLine(ex.StackTrace + Environment.NewLine);
                        Console.WriteLine("Program went kaboom. Oh well. Press any key to give up on life.");
                        Console.Read();
                    }
                }
                else
                    Console.WriteLine($"The source directory \"{src}\" is not a valid Blu-ray structure directory.");
            }
            else
                Console.WriteLine("Target directory not specified.");
        }
    }
}
